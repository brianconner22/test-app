FROM alpine
RUN apk --no-cache upgrade
RUN apk add --no-cache apache2
RUN echo "<html><body><h1>Hello Northeast Airlines!</h1></body></html>" > /var/www/localhost/htdocs/index.html
EXPOSE 80
CMD ["-D","FOREGROUND"]
ENTRYPOINT ["/usr/sbin/httpd"]
